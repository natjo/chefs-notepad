package routes

import (
	"image"
	"log/slog"
	"net/http"
	"net/url"

	"github.com/go-chi/chi/v5"
	"gitlab.com/natjo/chefs-notepad/config"
	"gitlab.com/natjo/chefs-notepad/recipeManager"
)

func GetTitleImageHandler(config *config.Config) func(http.ResponseWriter, *http.Request) {
	return templateGetTitleImageHandler(config, false)
}

func GetSmallTitleImageHandler(config *config.Config) func(http.ResponseWriter, *http.Request) {
	return templateGetTitleImageHandler(config, true)
}

func templateGetTitleImageHandler(config *config.Config, returnSmallImage bool) func(http.ResponseWriter, *http.Request) {
	recipeManager := recipeManager.NewRecipeManager(config.DataDir, config.CacheDir)
	return func(w http.ResponseWriter, r *http.Request) {
		// Get image path
		urlCategory := chi.URLParam(r, "category")
		urlRecipe := chi.URLParam(r, "recipe")
		category, _ := url.QueryUnescape(urlCategory)
		recipe, _ := url.QueryUnescape(urlRecipe)

		// Return image

		var img image.Image
		var err error
		if returnSmallImage {
			img, err = recipeManager.GetSmallTitleImage(category, recipe)
		} else {
			img, err = recipeManager.GetTitleImage(category, recipe)
		}
		if err != nil {
			slog.Error(err.Error())
			writeError(w, "Could not find image")
		} else {
			writeImage(w, &img)
		}
	}
}
