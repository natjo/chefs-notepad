package routes

import (
	"net/http"

	"gitlab.com/natjo/chefs-notepad/config"
)

func GetNotFoundHandler(config *config.Config) func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		generalValues := NewGeneralValues(config)
		renderTemplate(w, "templates/pages/notFound.tmpl.html", generalValues, nil)
	}
}
