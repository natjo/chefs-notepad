package routes

// This renders the inner part of the recipe page, i.e. the markdown text, or the html from the markdown text

import (
	"fmt"
	"html/template"
	"log/slog"
	"net/http"
	"net/url"

	"github.com/go-chi/chi/v5"
	"gitlab.com/natjo/chefs-notepad/config"
	"gitlab.com/natjo/chefs-notepad/recipeManager"
)

// Returns the recipe in html
func GetRecipeTextHandler(config *config.Config) func(http.ResponseWriter, *http.Request) {
	recipeManager := recipeManager.NewRecipeManager(config.DataDir, config.CacheDir)
	return func(w http.ResponseWriter, r *http.Request) {
		// Generate rendered recipe text
		urlCategory := chi.URLParam(r, "category")
		urlRecipe := chi.URLParam(r, "recipe")
		category, _ := url.QueryUnescape(urlCategory)
		recipe, _ := url.QueryUnescape(urlRecipe)
		recipeText, infoTags, err := recipeManager.GetRecipeText(category, recipe, false)

		// Return text
		if err != nil {
			writeError(w, "Could not find recipe")
		} else {
			renderRecipeArticle(w, category, recipe, recipeText, infoTags)
		}
	}
}

func GetRecipeTextEditHandler(config *config.Config) func(http.ResponseWriter, *http.Request) {
	recipeManager := recipeManager.NewRecipeManager(config.DataDir, config.CacheDir)
	return func(w http.ResponseWriter, r *http.Request) {
		// Generate markdown recipe text
		urlCategory := chi.URLParam(r, "category")
		urlRecipe := chi.URLParam(r, "recipe")
		category, _ := url.QueryUnescape(urlCategory)
		recipe, _ := url.QueryUnescape(urlRecipe)
		recipeText, _, err := recipeManager.GetRecipeText(category, recipe, true)

		// Return text
		if err != nil {
			writeError(w, "Could not find recipe")
		} else {
			renderChunk(w, "templates/chunks/recipeEditForm.tmpl.html", struct {
				Recipe     string
				Category   string
				RecipeText string
			}{
				Recipe:     recipe,
				Category:   category,
				RecipeText: recipeText,
			})
		}
	}
}

func PostRecipeTextHandler(config *config.Config) func(http.ResponseWriter, *http.Request) {
	recipeManager := recipeManager.NewRecipeManager(config.DataDir, config.CacheDir)
	return func(w http.ResponseWriter, r *http.Request) {
		// Check authentication first
		encryptedPassword := r.Header.Get("AccessToken")
		if !config.VerifyEncryptedPassword(encryptedPassword) {
			writeText(w, 401, "Not authenticated")
			return
		}

		// Generate rendered recipe text
		urlCategory := chi.URLParam(r, "category")
		urlRecipe := chi.URLParam(r, "recipe")
		category, _ := url.QueryUnescape(urlCategory)
		recipe, _ := url.QueryUnescape(urlRecipe)

		// And store recipe/title/image
		// Parse our multipart form, 10 << 20 specifies a maximum upload of 10 MB files
		err := r.ParseMultipartForm(10 << 20)
		if err != nil {
			slog.Error(err.Error())
			writeText(w, 500, "Internal error")
		}

		file, header, err := r.FormFile("titleImage")
		if err == nil {
			// New title image was uploaded
			recipeManager.SaveTitleImage(category, recipe, file, header)
			file.Close()
		} else {
			slog.Error(err.Error())
		}

		recipeText := r.Form.Get("recipe")
		recipeManager.SaveRecipeText(category, recipe, recipeText)
		recipeText, infoTags, err := recipeManager.GetRecipeText(category, recipe, false)

		// Optionally, rename the recipe
		newRecipeName := r.Form.Get("recipeName")
		wasRecipeRenamed := recipeManager.UpdateRecipeName(recipe, newRecipeName, category)

		// Return text
		if err != nil {
			writeError(w, "Could not find recipe")
		} else {
			if wasRecipeRenamed {
				w.Header().Set("HX-Redirect", fmt.Sprintf("/%s/%s", url.QueryEscape(category), url.QueryEscape(newRecipeName)))
				writeText(w, 200, "Renamed recipe")
			} else {
				renderRecipeArticle(w, category, recipe, recipeText, infoTags)
			}
		}
	}
}

func renderRecipeArticle(w http.ResponseWriter, category, recipe, recipeText string, infoTags map[string]string) {
	renderChunk(w, "templates/chunks/recipeArticle.tmpl.html", struct {
		Recipe     string
		Category   string
		RecipeText template.HTML
		InfoTags   map[string]string
	}{
		Recipe:     recipe,
		Category:   category,
		RecipeText: template.HTML(recipeText),
		InfoTags:   infoTags,
	})
}
