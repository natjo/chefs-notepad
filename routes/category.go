package routes

import (
	"net/http"
	"net/url"

	"github.com/go-chi/chi/v5"
	"gitlab.com/natjo/chefs-notepad/config"
	"gitlab.com/natjo/chefs-notepad/recipeManager"
)

type CategoryValues struct {
	Category string
	Recipes  []CategoryRecipe
}
type CategoryRecipe struct {
	Name           string
	TitleImagePath string
}

func GetCategoryHandler(config *config.Config) func(http.ResponseWriter, *http.Request) {
	recipeManager := recipeManager.NewRecipeManager(config.DataDir, config.CacheDir)
	return func(w http.ResponseWriter, r *http.Request) {
		urlCategory := chi.URLParam(r, "category")
		category, _ := url.QueryUnescape(urlCategory)
		recipeNames, err := recipeManager.GetRecipeNames(category)
		if err != nil {
			// Recipe not found
			GetNotFoundHandler(config)(w, r)
		}
		recipes := []CategoryRecipe{}
		for _, name := range recipeNames {
			titleImagePath := recipeManager.GetSmallTitleImageURLPath(category, name)
			recipes = append(recipes, CategoryRecipe{Name: name, TitleImagePath: titleImagePath})
		}

		generalValues := NewGeneralValues(config)
		generalValues.Title = category
		renderTemplate(w, "templates/pages/category.tmpl.html", generalValues, CategoryValues{Category: category, Recipes: recipes})
	}
}
