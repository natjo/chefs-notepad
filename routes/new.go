package routes

import (
	"fmt"
	"log/slog"
	"net/http"
	"net/url"

	"github.com/go-chi/chi/v5"
	"gitlab.com/natjo/chefs-notepad/config"
	"gitlab.com/natjo/chefs-notepad/recipeManager"
)

func GetNewHandler(config *config.Config) func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		urlCategory := chi.URLParam(r, "category")
		category, _ := url.QueryUnescape(urlCategory)

		generalValues := NewGeneralValues(config)
		renderTemplate(w, "templates/pages/new.tmpl.html", generalValues,
			struct {
				Category string
			}{
				Category: category,
			})
	}
}

func PostNewHandler(config *config.Config) func(http.ResponseWriter, *http.Request) {
	recipeManager := recipeManager.NewRecipeManager(config.DataDir, config.CacheDir)
	return func(w http.ResponseWriter, r *http.Request) {
		// Check authentication first
		encryptedPassword := r.Header.Get("AccessToken")
		if !config.VerifyEncryptedPassword(encryptedPassword) {
			writeText(w, 401, "Not authenticated")
			return
		}

		// Generate new recipe entry
		urlCategory := chi.URLParam(r, "category")
		category, _ := url.QueryUnescape(urlCategory)

		err := r.ParseForm()
		if err != nil {
			slog.Error(err.Error())
			writeText(w, 500, "Internal error")
		}

		recipeName := r.Form.Get("recipeName")
		slog.Info(fmt.Sprintf("Create new recipe with name \"%s\" in category \"%s\"", recipeName, category))
		success := recipeManager.CreateNewRecipe(category, recipeName)
		if !success {
			writeText(w, 302, "Rezept existiert bereits")
		} else {
			// Redirect user to new page
			w.Header().Set("HX-Redirect", fmt.Sprintf("/%s/%s", url.QueryEscape(category), url.QueryEscape(recipeName)))
			writeText(w, 200, "Creating new page")
		}
	}
}
