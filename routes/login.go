package routes

import (
	"fmt"
	"log/slog"
	"net/http"

	"gitlab.com/natjo/chefs-notepad/config"
)

func GetLoginHandler(config *config.Config) func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		generalValues := NewGeneralValues(config)
		renderTemplate(w, "templates/pages/login.tmpl.html", generalValues, nil)
	}
}

func PostLoginHandler(config *config.Config) func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		err := r.ParseForm()
		if err != nil {
			slog.Error(err.Error())
			writeText(w, 500, "Internal error")
		}
		password := r.Form.Get("password")
		if config.VerifyPassword(password) {
			// Store the token
			encryptedPassword := config.EncryptPassword(password)
			writeText(w, 200, fmt.Sprintf(`Correct Password! <script>localStorage.setItem("AccessToken", "%s")</script> <div hx-swap-oob="true" id="login-form"></div>`, encryptedPassword))
		} else {
			writeText(w, 401, "Wrong password")
		}
	}
}
