package routes

import (
	"fmt"
	"log/slog"

	"net/http"
	"net/url"

	"github.com/go-chi/chi/v5"
	"gitlab.com/natjo/chefs-notepad/config"
	"gitlab.com/natjo/chefs-notepad/recipeManager"
)

type RecipeValues struct {
	TitleImagePath string
	Category       string
	Recipe         string
	LinkPreview    LinkPreview
}

type LinkPreview struct {
	Title     string
	ImagePath string
	Url       string
}

func GetRecipeHandler(config *config.Config) func(http.ResponseWriter, *http.Request) {
	recipeManager := recipeManager.NewRecipeManager(config.DataDir, config.CacheDir)
	return func(w http.ResponseWriter, r *http.Request) {
		urlCategory := chi.URLParam(r, "category")
		urlRecipe := chi.URLParam(r, "recipe")
		category, _ := url.QueryUnescape(urlCategory)
		recipe, _ := url.QueryUnescape(urlRecipe)
		if !recipeManager.ExistsRecipe(category, recipe) {
			GetNotFoundHandler(config)(w, r)
			return
		}
		titleImagePath := recipeManager.GetTitleImageURLPath(category, recipe)
		generalValues := NewGeneralValues(config)
		generalValues.Title = recipe
		generalValues.Category = category
		generalValues.Recipe = recipe
		renderTemplate(w, "templates/pages/recipe.tmpl.html", generalValues,
			RecipeValues{
				TitleImagePath: titleImagePath,
				Category:       category,
				Recipe:         recipe,
				LinkPreview: LinkPreview{
					Title:     generalValues.Title,
					ImagePath: titleImagePath,
					Url:       getUrlFromRequest(r),
				},
			})
	}
}

func DeleteRecipeHandler(config *config.Config) func(http.ResponseWriter, *http.Request) {
	recipeManager := recipeManager.NewRecipeManager(config.DataDir, config.CacheDir)
	return func(w http.ResponseWriter, r *http.Request) {
		// Check authentication first
		encryptedPassword := r.Header.Get("AccessToken")
		if !config.VerifyEncryptedPassword(encryptedPassword) {
			writeText(w, 401, "Not authenticated")
			return
		}

		// Collect parameters
		urlCategory := chi.URLParam(r, "category")
		urlRecipe := chi.URLParam(r, "recipe")
		category, _ := url.QueryUnescape(urlCategory)
		recipe, _ := url.QueryUnescape(urlRecipe)

		// Ensure recipe exists
		if !recipeManager.ExistsRecipe(category, recipe) {
			GetNotFoundHandler(config)(w, r)
			return
		}

		slog.Info("Delete Recipe", "Category", category, "Recipe", recipe)
		recipeManager.DeleteRecipe(category, recipe)
		w.Header().Set("HX-Redirect", fmt.Sprintf("/%s", url.QueryEscape(category)))
		writeText(w, 200, "Deleted recipe")
	}
}

func getUrlFromRequest(r *http.Request) string {
	return fmt.Sprintf("%s://%s", getScheme(r), r.Host)
}

func getScheme(r *http.Request) string {
	if r.TLS != nil {
		return "https"
	}
	return "http"
}
