package routes

import (
	"net/http"

	"gitlab.com/natjo/chefs-notepad/config"
	"gitlab.com/natjo/chefs-notepad/recipeManager"
)

type IndexValues struct {
	Categories []string
}

func GetIndexHandler(config *config.Config) func(http.ResponseWriter, *http.Request) {
	recipeManager := recipeManager.NewRecipeManager(config.DataDir, config.CacheDir)
	return func(w http.ResponseWriter, r *http.Request) {
		categories := recipeManager.GetCategories()
		generalValues := NewGeneralValues(config)
		renderTemplate(w, "templates/pages/index.tmpl.html", generalValues, IndexValues{Categories: categories})
	}
}
