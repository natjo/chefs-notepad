package routes

import (
	"github.com/go-chi/chi/v5"
	"gitlab.com/natjo/chefs-notepad/config"
)

func Setup(r *chi.Mux, config *config.Config) {
	r.Get("/", GetIndexHandler(config))
	r.Get("/help", GetHelpHandler(config))
	r.Get("/login", GetLoginHandler(config))
	r.Post("/login", PostLoginHandler(config))

	r.Get("/{category}", GetCategoryHandler(config))

	r.Get("/{category}/new", GetNewHandler(config))
	r.Post("/{category}/new", PostNewHandler(config))

	r.Get("/{category}/{recipe}", GetRecipeHandler(config))
	r.Delete("/{category}/{recipe}", DeleteRecipeHandler(config))
	r.Get("/{category}/{recipe}/title_image.*", GetTitleImageHandler(config))
	r.Get("/{category}/{recipe}/title_image_small.*", GetSmallTitleImageHandler(config))
	r.Get("/{category}/{recipe}/text", GetRecipeTextHandler(config))
	r.Post("/{category}/{recipe}/text", PostRecipeTextHandler(config))
	r.Get("/{category}/{recipe}/text/edit", GetRecipeTextEditHandler(config))

	r.NotFound(GetNotFoundHandler(config))
}
