package routes

import (
	"bytes"
	"encoding/json"
	"fmt"
	"html/template"
	"image"
	"image/jpeg"
	"log/slog"
	"net/http"
	"strconv"

	"gitlab.com/natjo/chefs-notepad/config"
)

type GeneralValues struct {
	// Top level variables here are used in the base layout
	Title    string
	Category string
	Recipe   string
	// This will be forwarded to the content embedded within the base layout and can contain arbitrary values
	PageValues any
}

func NewGeneralValues(config *config.Config) *GeneralValues {
	return &GeneralValues{Title: config.Title}
}

// Renders entire page with the main layout
// General values is used in the general layout, while pageValues will be forwarded to the inner content
func renderTemplate(w http.ResponseWriter, htmlTemplate string, generalValues *GeneralValues, pageValues any) {
	t := template.Must(template.ParseFiles("templates/layouts/base.tmpl.html"))
	t = addIconTemplates(t)
	t, err := t.ParseFiles(htmlTemplate)
	if err != nil {
		slog.Error(err.Error())
	}
	generalValues.PageValues = pageValues
	err = t.Execute(w, generalValues)
	if err != nil {
		slog.Error(err.Error())
		writeText(w, 500, "Internal error")
	}
}

// Renders only a chunk of a full html page
func renderChunk(w http.ResponseWriter, htmlChunkTemplate string, values any) {
	t := template.Must(template.ParseFiles(htmlChunkTemplate))
	t = addIconTemplates(t)
	err := t.Execute(w, values)
	if err != nil {
		slog.Error(err.Error())
		writeText(w, 500, "Internal error")
	}
}

func addIconTemplates(t *template.Template) *template.Template {
	return template.Must(t.ParseGlob("templates/icons/*.tmpl.html"))
}

func writeText(w http.ResponseWriter, statusCode int, text string) {
	w.WriteHeader(statusCode)
	_, err := w.Write([]byte(text))
	if err != nil {
		slog.Error(err.Error())
	}
}

// func writeRedirect(w http.ResponseWriter, r *http.Request, url string) {
// 	http.Redirect(w, r, url, http.StatusMovedPermanently)
// }

// Encodes an image 'img' in jpeg format and writes it into ResponseWriter.
func writeImage(w http.ResponseWriter, img *image.Image) {
	buffer := new(bytes.Buffer)
	if err := jpeg.Encode(buffer, *img, nil); err != nil {
		slog.Error("unable to encode image")
	}

	w.Header().Set("Content-Type", "image/jpeg")
	w.Header().Set("Content-Length", strconv.Itoa(len(buffer.Bytes())))
	if _, err := w.Write(buffer.Bytes()); err != nil {
		slog.Error("unable to write image.")
	}
}

// TODO refactor to use custom error
func writeError(w http.ResponseWriter, message string) {
	w.WriteHeader(http.StatusNotFound)
	w.Header().Set("Content-Type", "application/json")
	resp := make(map[string]string)
	resp["message"] = message
	jsonResp, err := json.Marshal(resp)
	if err != nil {
		slog.Error(fmt.Sprintf("Error happened in JSON marshal. Err: %s", err.Error()))
	}
	_, err = w.Write(jsonResp)
	if err != nil {
		slog.Error(err.Error())
	}
}
