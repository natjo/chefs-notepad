module gitlab.com/natjo/chefs-notepad

go 1.21

require (
	github.com/go-chi/chi/v5 v5.0.11
	github.com/go-chi/render v1.0.3
	github.com/joho/godotenv v1.5.1
	github.com/stretchr/testify v1.8.4
	golang.org/x/image v0.15.0
)

require (
	github.com/ajg/form v1.5.1 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
