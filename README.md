# Chef's Notepad
Structure based on https://github.com/claidler/htmx_go_tailwind_starter


## Configuration
Either through environment variables, or a `.env` file in the working directory.

| Environment variable | Usage                         | Default                  | Required |
| -------------------- | ----------------------------- | ------------------------ | -------- |
| PASSWORD             | For login to the platform     | -                        | Yes      |
| DATA_DIR             | Path to the recipe directory  | -                        | Yes      |
| TITLE                | Title of the page             | Chef's Notepad           | No       |
| CACHE_DIR            | Path to the caching directory | /tmp/chefs-notepad/cache | No       |

## How to use from released archive
- Download archive
- Extract with `tar -xzf server.tar.gz`
- Create `.env`, see Configuration for details
- Run `chefs-notepad`

The install process is even simpler with [the script](https://gitlab.com/natjo/chefs-notepad/-/snippets/3687826) provided within this repository.

## Development
### Prerequisits for Development
You need 
- [tailwind cli](https://github.com/tailwindlabs/tailwindcss/releases/latest)
- [browser-sync](https://browsersync.io/)
- [air](https://github.com/cosmtrek/air)

### Run development server
Run `make dev` to run the server. It automatically runs autorefresh if you make changes

### Lint
To replicate the pipelines linting, simply run
```
docker run --rm -v $(pwd):/app -w /app golangci/golangci-lint:v1.58.1 golangci-lint run -v
```

## Notes
- Icons originally from https://heroicons.com/solid