package main

import (
	"log/slog"
	"net/http"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"

	"gitlab.com/natjo/chefs-notepad/config"
	"gitlab.com/natjo/chefs-notepad/routes"
)

func main() {
	config := config.NewConfig()

	r := chi.NewRouter()
	r.Use(middleware.Logger)

	publicFs := http.FileServer(http.Dir("static"))
	r.Handle("/images/*", publicFs)
	r.Handle("/styles/*", publicFs)
	r.Handle("/js/*", publicFs)

	routes.Setup(r, config)

	slog.Info("Listening on port 3000")
	err := http.ListenAndServe(":3000", r)
	if err != nil {
		slog.Error(err.Error())
	}
}
