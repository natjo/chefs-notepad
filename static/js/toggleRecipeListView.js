function toggleRecipeListView() {
  // Each time this function is called, it will replace all elements of the container with the next column
  const containerName = "recipes-container";
  const columnClasses = [
    ["columns-2", "text-sm", "px-[10%]", "gap-8", "font-semibold"],
    ["columns-sm", "text-lg", "px-[20%]", "gap-8", "font-bold"],
    ["columns-3", "text-xs", "px-[5%]", "gap-4", "font-semibold"],
  ];
  const e = document.getElementById(containerName);

  const [columnClasses0, columnClasses1] = getCurrentColumnClasses(
    e,
    columnClasses
  );

  for (let i = 0; i < columnClasses0.length; i++) {
    replaceClass(e, columnClasses0[i], columnClasses1[i]);
  }
}

function getCurrentColumnClasses(element, columnClasses) {
  for (let i = 0; i < columnClasses.length; i++) {
    console.log(i);
    if (element.classList.contains(columnClasses[i][0])) {
      // Return the first index if the current index is the last one
      const iNext = (i + 1) % columnClasses.length;
      return [columnClasses[i], columnClasses[iNext]];
    }
  }
}

function replaceClass(element, original, update) {
  if (original !== update) {
    element.classList.remove(original);
    element.classList.add(update);
  }
}
