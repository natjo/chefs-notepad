// We set elements to invisible and 0 width and height to hide them. To later show them again, we undo this
function filterRecipes(searchName) {
  let recipes = document.getElementById("recipes-container").children;
  for (let recipe of recipes) {
    if (containsString(recipe, searchName)) {
      hideRecipe(recipe);
    } else {
      showRecipe(recipe);
    }
  }
}

function containsString(originalStr, searchStr) {
  return (
    // Only filter on more than 2 letters in the search name
    searchStr.length > 1 &&
    !originalStr.id.toLowerCase().includes(searchStr.toLowerCase())
  );
}

function hideRecipe(recipe) {
  recipe.classList.add("hidden");
}

function showRecipe(recipe) {
  recipe.classList.remove("hidden");
}
