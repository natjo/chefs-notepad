var shownPopover = ""

function positionAllPopovers() {
  const popovers = document.getElementsByClassName("popover-indicator");
  for (var i = 0; i < popovers.length; i++) {
    popovers[i].addEventListener("click", toggleAndCenterPopover);
  }
}

function toggleAndCenterPopover(event) {
  const invoker = event.target;
  const popover = document.getElementById(invoker.value);

  window.FloatingUIDOM.computePosition(invoker, popover, {
    placement: "top",
  }).then(({ x, y }) => {
    // Uodate popover indexes
    Object.assign(popover.style, {
      left: `${x}px`,
      top: `${y}px`,
    });
  });
    popover.togglePopover()
}