function showHeaderNavButton() {
  // Only show the edit button if the user is logged in, i.e. when AccessToken is set
  if (localStorage.getItem("AccessToken") != null) {
    // Header
    showElement("header-nav-button-container");

    // Login form
    hideElement("login-form");
    setInnerHtml("login-message", "You are already logged in");

    // Category View
    showElement("new-recipe-button");
  }else{
    hideElement("new-recipe-button")
  }
}

function hideElement(id) {
  element = document.getElementById(id);
  if (element) {
    element.style.visibility = "hidden";
  }
}

function showElement(id) {
  element = document.getElementById(id);
  if (element) {
    element.style.visibility = "visible";
  }
}

function setInnerHtml(id, content) {
  element = document.getElementById(id);
  if (element) {
    element.innerHTML = content;
  }
}

function onLoad() {
  showHeaderNavButton();
}

window.onload = onLoad;
