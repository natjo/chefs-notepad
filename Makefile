.PHONY: run dev test

run:
	go run .

# Run air, tailwind and browser-sync
dev:
	air -c ./.air.toml & \
	npx browser-sync start \
	    --files "./templates/**/*.html" \
	    --port 3001 \
		--no-notify \
	    --proxy 'localhost:3000' \
	    --middleware 'function(req, res, next) { \
	      res.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); \
	      return next(); \
	    }' & \
	npx @tailwindcss/cli -i styles.css -o static/styles/styles.css --watch

test:
	go test ./...

test-color:
	grc go test ./...
