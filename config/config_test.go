package config

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestVerifyPassword(t *testing.T) {
	// Given
	testPassword := "abc123!?<>"
	config := NullConfig()
	config.EncryptedPassword = encryptPassword(testPassword)

	// When
	isCorrect := config.VerifyPassword(testPassword)
	isWrong := config.VerifyPassword("not-the-password")

	// Then
	assert.True(t, isCorrect)
	assert.False(t, isWrong)
}

func TestVerifyEncryptedPassword(t *testing.T) {
	// Given
	testPassword := "abc123!?<>"
	config := NullConfig()
	config.EncryptedPassword = encryptPassword(testPassword)

	// When
	isCorrect := config.VerifyEncryptedPassword(encryptPassword(testPassword))
	isWrong := config.VerifyEncryptedPassword(encryptPassword("not-the-password"))

	// Then
	assert.True(t, isCorrect)
	assert.False(t, isWrong)
}
