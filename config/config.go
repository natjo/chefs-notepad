package config

import (
	"encoding/base64"
	"fmt"
	"log/slog"
	"os"

	"github.com/joho/godotenv"
)

type Config struct {
	Title             string
	DataDir           string
	CacheDir          string
	EncryptedPassword string
}

func NewConfig() *Config {
	err := godotenv.Load()
	if err != nil {
		slog.Error("Error loading .env file")
	}
	password := getFromEnvRequired("PASSWORD")
	dataDir := getFromEnvRequired("DATA_DIR")
	title := getFromEnv("TITLE", "Chef's Notepad")
	cacheDir := getFromEnv("CACHE_DIR", "/tmp/chefs-notepad/cache")
	return &Config{Title: title, DataDir: dataDir, CacheDir: cacheDir, EncryptedPassword: encryptPassword(password)}
}

// Empty config for testing
func NullConfig() *Config {
	return &Config{}
}

func (c *Config) VerifyPassword(password string) bool {
	return c.VerifyEncryptedPassword(c.EncryptPassword(password))
}

func (c *Config) VerifyEncryptedPassword(encryptedPassword string) bool {
	return encryptedPassword == c.EncryptedPassword
}

func (c *Config) EncryptPassword(password string) string {
	return encryptPassword(password)
}

func encryptPassword(password string) string {
	return base64.StdEncoding.EncodeToString([]byte(password))
}

func getFromEnv(envName, defaultValue string) string {
	value, ok := os.LookupEnv(envName)
	if !ok {
		return defaultValue
	}
	return value
}

func getFromEnvRequired(envName string) string {
	value, ok := os.LookupEnv(envName)
	if !ok {
		slog.Error(fmt.Sprintf("Environment variable \"%s\" not set", envName))
		os.Exit(1)
	}
	return value
}
