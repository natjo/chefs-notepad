package recipeManager

import (
	"io"
	"log/slog"
	"mime/multipart"
	"os"
)

func copyFile(src, target string) {
	r, err := os.Open(src)
	if err != nil {
		panic(err)
	}
	defer r.Close()
	w, err := os.Create(target)
	if err != nil {
		panic(err)
	}
	defer w.Close()
	_, err = w.ReadFrom(r)
	if err != nil {
		slog.Error(err.Error())
	}
}

func storeFile(path string, newImage multipart.File) {
	imageFile, err := os.OpenFile(path, os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0755)
	if err != nil {
		slog.Error(err.Error())
		return
	}
	defer imageFile.Close()

	fileBytes, err := io.ReadAll(newImage)
	if err != nil {
		slog.Error(err.Error())
		return
	}
	_, err = imageFile.Write(fileBytes)
	if err != nil {
		slog.Error(err.Error())
	}
}

func deleteFile(path string) {
	err := os.Remove(path)
	if err != nil {
		slog.Error(err.Error())
	}
}

func replaceAtPosition(originalText, newText string, start, end int) string {
	partTwo := ""
	partOne := ""
	if start >= 0 {
		partOne = originalText[:start]
	}
	if end < len(originalText) {
		partTwo = originalText[end:]
	}
	return partOne + newText + partTwo
}

func isLetter(r rune) bool {
	if (r < 'a' || r > 'z') && (r < 'A' || r > 'Z') {
		return false
	}
	return true
}
