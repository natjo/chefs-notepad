package recipeManager

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGetOnlyPlainSegment(t *testing.T) {
	// Given
	markdown := "a b"

	// When
	textSegments := getTextSegments(markdown)

	// Then
	expected := []*TextSegment{NewTextSegment("a b", PLAIN)}
	assert.Equal(t, expected, textSegments)
}

func TestGetOnlyItalicSegment(t *testing.T) {
	// Given
	markdown := "_a b_"

	// When
	textSegments := getTextSegments(markdown)

	// Then
	expected := []*TextSegment{NewTextSegment("a b", ITALIC)}
	assert.Equal(t, expected, textSegments)
}

func TestGetOnlyBoldSegment(t *testing.T) {
	// Given
	markdown := "*a b*"

	// When
	textSegments := getTextSegments(markdown)

	// Then
	expected := []*TextSegment{NewTextSegment("a b", BOLD)}
	assert.Equal(t, expected, textSegments)
}

func TestGetPartlyItalicSegment(t *testing.T) {
	// Given
	markdown := "a _b c_"

	// When
	textSegments := getTextSegments(markdown)

	// Then
	expected := []*TextSegment{NewTextSegment("a ", PLAIN), NewTextSegment("b c", ITALIC)}
	assert.Equal(t, expected, textSegments)
}

func TestGetPartlyBoldSegment(t *testing.T) {
	// Given
	markdown := "a *b c*"

	// When
	textSegments := getTextSegments(markdown)

	// Then
	expected := []*TextSegment{NewTextSegment("a ", PLAIN), NewTextSegment("b c", BOLD)}
	assert.Equal(t, expected, textSegments)
}

func TestGetBoldAndItalicSegment(t *testing.T) {
	// Given
	markdown := "a *b c* d_ e f_ g"

	// When
	textSegments := getTextSegments(markdown)

	// Then
	expected := []*TextSegment{
		NewTextSegment("a ", PLAIN),
		NewTextSegment("b c", BOLD),
		NewTextSegment(" d", PLAIN),
		NewTextSegment(" e f", ITALIC),
		NewTextSegment(" g", PLAIN),
	}
	assert.Equal(t, expected, textSegments)
}

func TestGetUnfinishedItalicSegment(t *testing.T) {
	// Given
	markdown := "a_b *c d*"

	// When
	textSegments := getTextSegments(markdown)

	// Then
	expected := []*TextSegment{
		NewTextSegment("a_b ", PLAIN),
		NewTextSegment("c d", BOLD),
	}
	assert.Equal(t, expected, textSegments)
}

func TestGetUnfinishedBoldSegment(t *testing.T) {
	// Given
	markdown := "*_a b* *c_ d"

	// When
	textSegments := getTextSegments(markdown)

	// Then
	expected := []*TextSegment{
		NewTextSegment("a b", BOLD_AND_ITALIC),
		NewTextSegment(" *c", ITALIC),
		NewTextSegment(" d", PLAIN),
	}
	assert.Equal(t, expected, textSegments)
}

func TestGetBoldAndItalicCombinedSegment(t *testing.T) {
	// Given
	markdown := "a *b _c_*"

	// When
	textSegments := getTextSegments(markdown)

	// Then
	expected := []*TextSegment{
		NewTextSegment("a ", PLAIN),
		NewTextSegment("b ", BOLD),
		NewTextSegment("c", BOLD_AND_ITALIC),
	}
	assert.Equal(t, expected, textSegments)
}
