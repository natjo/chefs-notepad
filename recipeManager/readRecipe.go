package recipeManager

import (
	"bufio"
	"os"
)

func readRecipe(recipeFile *os.File) string {
	mdText := ""
	scanner := bufio.NewScanner(recipeFile)
	for scanner.Scan() {
		line := scanner.Text()
		mdText += line + "\n"
	}
	return mdText
}
