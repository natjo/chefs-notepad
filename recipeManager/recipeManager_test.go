package recipeManager

import (
	"regexp"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGetCategories(t *testing.T) {
	// Given
	mgr := NewRecipeManager("./../dev/test-data", "/tmp/chefs-notepad/cache")

	// When
	result := mgr.GetCategories()

	// Then
	assert.ElementsMatch(t, []string{"category0", "Category 1", "Category 2"}, result, "Should be correct categories as found in ./dev/test-data")
}

func TestGetRecipes(t *testing.T) {
	// Given
	mgr := NewRecipeManager("./../dev/test-data", "/tmp/chefs-notepad/cache")

	// When
	result0, _ := mgr.GetRecipeNames("category0")
	result1, _ := mgr.GetRecipeNames("Category 1")

	// Then
	assert.ElementsMatch(t, []string{"recipe-0"}, result0, "Should be correct recipes as found in ./dev/test-data/category0")
	assert.ElementsMatch(t, []string{"Another-Recipe", "Tasty Recipe"}, result1, "Should be correct recipes as found in ./dev/test-data/Category1")
}

func TestGetRecipesNotExistingCategory(t *testing.T) {
	// Given
	mgr := NewRecipeManager("./../dev/test-data", "/tmp/chefs-notepad/cache")

	// When
	_, err := mgr.GetRecipeNames("category2")

	// Then
	assert.ErrorContains(t, err, "no such file or directory")
}

func TestGetRecipesInvalidCategory(t *testing.T) {
	// Given
	mgr := NewRecipeManager("./../dev/test-data", "/tmp/chefs-notepad/cache")

	// When
	_, err := mgr.GetRecipeNames("../../home")

	// Then
	assert.ErrorContains(t, err, "invalid Category")
}

func TestGetTitleImageURLPath(t *testing.T) {
	// Given
	mgr := NewRecipeManager("./../dev/test-data", "/tmp/chefs-notepad/cache")

	// When
	path := mgr.GetTitleImageURLPath("Category_1", "Tasty Recipe")

	// Then
	assert.Regexp(t, regexp.MustCompile(`/Category_1/Tasty_Recipe/title_image\.jpg\?\d*`), path)
}

func TestExistsRecipeTrue(t *testing.T) {
	// Given
	mgr := NewRecipeManager("./../dev/test-data", "/tmp/chefs-notepad/cache")

	// When
	exists := mgr.ExistsRecipe("category0", "recipe-0")

	// Then
	assert.True(t, exists)
}

func TestExistsRecipeFalse(t *testing.T) {
	// Given
	mgr := NewRecipeManager("./../dev/test-data", "/tmp/chefs-notepad/cache")

	// When
	exists := mgr.ExistsRecipe("category123", "recipe-0")

	// Then
	assert.False(t, exists)
}
