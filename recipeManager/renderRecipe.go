package recipeManager

import (
	"bufio"
	"fmt"
	"os"
	"regexp"
	"strings"
)

// We allow language specific characters, too. This can be extended for more languages
const quantityInfoLabelRegex = `\(\([\wäÄöÖüÜßøØæÆ ]*:[\wäÄöÖüÜßøØæÆ ]*\)\)$`

// To keep things simple, we always look only at the current line when iterating over the markdown
// This means we can't create things like nested lists or codeblocks
func renderRecipe(recipeFile *os.File) (string, map[string]string) {
	htmlText := NewHtmlText()
	infoTags := map[string]string{}
	scanner := bufio.NewScanner(recipeFile)
	for scanner.Scan() {
		line := scanner.Text()
		handleLine(line, htmlText, infoTags)
	}
	return htmlText.GetText(), infoTags
}

func handleLine(line string, htmlText *HtmlText, infoTags map[string]string) {
	// We ignore preceding whitelines to be more forgiving.
	// This causes that we can't creat nested lists and code blocks
	line = strings.TrimSpace(line)
	switch {
	// Headlines
	case strings.HasPrefix(line, "####"):
		text := strings.TrimPrefix(line, "####")
		textSegments := getTextSegments(text)
		htmlText.AppendH4(textSegments)

	case strings.HasPrefix(line, "###"):
		text := strings.TrimPrefix(line, "###")
		textSegments := getTextSegments(text)
		htmlText.AppendH3(textSegments)

	case strings.HasPrefix(line, "##"):
		text := strings.TrimPrefix(line, "##")
		textSegments := getTextSegments(text)
		htmlText.AppendH2(textSegments)

	case strings.HasPrefix(line, "#"):
		text := strings.TrimPrefix(line, "#")
		textSegments := getTextSegments(text)
		htmlText.AppendH1(textSegments)

	// divider
	case strings.HasPrefix(line, "---"):
		htmlText.AppendDivider()

	// List
	// On lists we also check if quantity and gredient are specified
	case strings.HasPrefix(line, "-"):
		text := strings.TrimPrefix(line, "-")
		updateQuantityInfo(htmlText, text)
		text = removeQuantityInfo(text)
		textSegments := getTextSegments(text)
		htmlText.AppendListItem(textSegments)

	// info tag
	case strings.HasPrefix(line, "[") && strings.HasSuffix(line, "]"):
		infoTagKey, infoTagValue, err := getInfoTag(line)
		if err == nil {
			infoTags[infoTagKey] = infoTagValue
		} else {
			// Couldn't understand format of info tag, just append as simple line
			textSegments := getTextSegments(line)
			htmlText.AppendP(textSegments)
		}

	// Ignore empty lines
	case len(line) == 0:

	// Default paragraph
	default:
		textSegments := getTextSegments(line)
		htmlText.AppendP(textSegments)
	}
}

func getInfoTag(line string) (string, string, error) {
	// We expect this format:
	// [key: value]
	// Any additional colons will be part of the value
	if strings.Count(line, "[") > 1 {
		return "", "", fmt.Errorf("too many square brackets")
	}
	if strings.Count(line, "]") > 1 {
		return "", "", fmt.Errorf("too many square brackets")
	}
	if strings.Count(line, ":") == 0 {
		return "", "", fmt.Errorf("missing collon")
	}

	indexColon := strings.Index(line, ":")
	key := strings.TrimSpace(line[1:indexColon])
	value := strings.TrimSpace(line[indexColon+1 : len(line)-1])
	return key, value, nil
}

func updateQuantityInfo(htmlText *HtmlText, text string) {
	quantity, ingredient, ok := getQuantity(text)
	if ok {
		htmlText.UpdateQuantityInfo(quantity, ingredient)
	}
}

func removeQuantityInfo(text string) string {
	re := regexp.MustCompile(quantityInfoLabelRegex)
	return re.ReplaceAllString(text, "")
}

// Returns item and quantity, it's very conservative. If it's not sure, it will return a false, indicating the value shouldn't be used
// The rules to find item and quantity are
// - Ignore everything after a special character
// - The last word separated by whitespace is the item. Everything before this is quantity
func getQuantity(text string) (string, string, bool) {
	if hasLabel(text) {
		quantity, ingredient, ok := getQuantityFromLabels(text)
		if !ok {
			return "", "", ok
		}
		// Check if not all labels where defined, e.g. ((:ingredient))
		if quantity == "" {
			quantity, _, ok = getQuantityWithoutLabels(text)
		}
		if ingredient == "" {
			_, ingredient, ok = getQuantityWithoutLabels(text)
		}
		return quantity, ingredient, ok
	}
	return getQuantityWithoutLabels(text)
}

func getQuantityWithoutLabels(text string) (string, string, bool) {
	// Remove everything after special character
	re := regexp.MustCompile(`[^\wäÄöÖüÜßøØæÆ\s.]`)
	text = re.Split(text, -1)[0]

	// Remove excess whitespace
	text = strings.TrimSpace(text)

	// Split by last whitespace, first part quantity, second part ingredient
	indexSeparator := strings.LastIndex(text, " ")
	if indexSeparator < 0 {
		return "", "", false
	}
	quantity := text[:indexSeparator]
	ingredient := text[indexSeparator+1:]

	return quantity, ingredient, true
}

func hasLabel(text string) bool {
	// Simplified regex for the matching, only checks for (()).
	// If this is detexed but wrongly formatted, we should ignore the label rather than
	// wrongly identifying it
	re := regexp.MustCompile(`\(\([^\)]*\)\)`)
	return re.MatchString(text)
}

func getQuantityFromLabels(text string) (string, string, bool) {
	re := regexp.MustCompile(quantityInfoLabelRegex)
	if !re.MatchString(text) {
		return "", "", false
	}
	// Check for label, it will have the format. We can simply split on : and remove the parantheses
	// ((quantity:ingredient))
	label := re.FindString(text)
	labelSplit := strings.Split(label, ":")
	quantity := labelSplit[0][2:]
	ingredient := labelSplit[1][:len(labelSplit[1])-2]

	return quantity, ingredient, true
}
