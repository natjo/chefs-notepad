package recipeManager

import (
	"fmt"
	"log/slog"
	"os"
	"path/filepath"
	"syscall"
	"time"
)

// Used to generate file paths
// Does no actual file interaction. We use it to ensure all code uses the same paths for the same structures
type PathManager struct {
	rootPath  string
	cachePath string
}

func NewPathManager(rootPath, cachePath string) *PathManager {
	return &PathManager{
		rootPath:  rootPath,
		cachePath: cachePath,
	}
}

func (mgr *PathManager) GetTextStoragePath(category, recipe string) string {
	relativeRecipePath := mgr.getRelativeRecipeDirectory(category, recipe)
	return filepath.Join(mgr.rootPath, relativeRecipePath, "recipe.md")
}

func (mgr *PathManager) GetTitleImageURLPath(category, recipe string) string {
	relativeRecipePath := mgr.getRelativeRecipeDirectory(category, recipe)
	imageName := mgr.getImageFilename(relativeRecipePath)
	return filepath.Join("/", relativeRecipePath, imageName)
}

func (mgr *PathManager) GetSmallTitleImageURLPath(category, recipe string) string {
	relativeRecipePath := mgr.getRelativeRecipeDirectory(category, recipe)
	imageName := mgr.getSmallImageFilename(category, recipe)
	return filepath.Join("/", relativeRecipePath, imageName)
}

func (mgr *PathManager) GetTitleImagePath(category, recipe string) string {
	relativeRecipePath := mgr.getRelativeRecipeDirectory(category, recipe)
	imageName := mgr.getImageFilename(relativeRecipePath)
	return filepath.Join(mgr.rootPath, relativeRecipePath, imageName)
}

func (mgr *PathManager) GetTitleImageCreationDate(category, recipe string) int64 {
	fi, err := os.Stat(mgr.GetTitleImagePath(category, recipe))
	if err != nil {
		slog.Error(fmt.Sprintf("Error when reading stats of file: %s", err.Error()))
		return 0
	}
	stat := fi.Sys().(*syscall.Stat_t)
	return time.Unix(int64(stat.Ctim.Sec), int64(stat.Ctim.Nsec)).Unix()
}

// This returns the path in a cache directory
// We have to use the extension here as we can only know this from the original
func (mgr *PathManager) GetSmallTitleImagePath(category, recipe string) string {
	relativeRecipePath := mgr.getRelativeRecipeDirectory(category, recipe)
	imageName := mgr.getSmallImageFilename(category, recipe)
	return filepath.Join(mgr.cachePath, relativeRecipePath, imageName)
}

// We have to use some regex, as we don't know the extension
func (mgr *PathManager) getImageFilename(relativeRecipePath string) string {
	imageName := "title_image"
	name := filepath.Join(mgr.rootPath, relativeRecipePath, imageName)
	// Allow any image file extension
	images, _ := filepath.Glob(name + ".*")
	return filepath.Base(images[0])
}

func (mgr *PathManager) getSmallImageFilename(category, recipe string) string {
	extension := mgr.getTitleImageExtension(category, recipe)
	return fmt.Sprintf("title_image_small%s", extension)
}

func (mgr *PathManager) getRelativeRecipeDirectory(category, recipe string) string {
	category = addUnderscoresToName(category)
	recipe = addUnderscoresToName(recipe)
	return filepath.Join(category, recipe)
}

func (mgr *PathManager) getAbsoluteRecipeDirectory(category, recipe string) string {
	relativePath := mgr.getRelativeRecipeDirectory(category, recipe)
	return filepath.Join(mgr.rootPath, relativePath)
}

// Since we can't know the extension by default, we have to look it up from the original title image
func (mgr *PathManager) getTitleImageExtension(category, recipe string) string {
	return filepath.Ext(mgr.GetTitleImagePath(category, recipe))
}
