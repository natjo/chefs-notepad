package recipeManager

import (
	"strings"
)

const ITALIC_INDICATOR = "_"
const BOLD_INDICATOR = "*"
const ALL_INDICATORS = "_*"

type TextSegment struct {
	Text   string
	Format Format
}

type Format int

const (
	BOLD Format = iota
	ITALIC
	BOLD_AND_ITALIC
	PLAIN
)

func NewTextSegment(text string, format Format) *TextSegment {
	return &TextSegment{Text: text, Format: format}
}

func getTextSegments(text string) []*TextSegment {
	textSegments := []*TextSegment{}
	text = strings.TrimSpace(text)

	// Define initial setup, we always start with a plain block
	index := 0
	format := PLAIN

	// Iterate over text, always finding the next indicator
	for {
		// Find next index
		newIndex, newFormat := findNextIndicator(text, index)

		// Check for stop condition
		if newIndex == -1 {
			// No indicators found, add final text segment
			textSegments = addTextSegment(textSegments, text[index:], PLAIN)
			break
		}

		// Only use new format indicator if it's not the last one or if it ends an existing format
		if endsFormat(format, newFormat) || containsFormatIndicator(text[newIndex+1:], newFormat) {
			// Add new textSegment
			textSegments = addTextSegment(textSegments, text[index:newIndex], format)

			// Update format and index
			format = updateFormat(format, newFormat)
		} else {
			// We basically add the same format as the past one here, just after the indicator that is only a normal letter.
			// This is fine, as the addTextSegment function directly extends the past segment
			// instead of adding a new one if the format is the same.
			// Plus one to include the indicator, as it's just a on purpose letter
			textSegments = addTextSegment(textSegments, text[index:newIndex+1], format)
		}
		// Add one to index to ignore the format indicator
		index = newIndex + 1

		// Check for stop condition
		if index >= len(text) {
			// End reached
			break
		}
	}
	return textSegments
}

func endsFormat(oldFormat, newFormat Format) bool {
	return oldFormat == BOLD_AND_ITALIC || oldFormat == newFormat
}

// Returns index and type of the next indicator found
func findNextIndicator(text string, startIndex int) (int, Format) {
	subString := text[startIndex:]

	// Check for next index of each indicator type
	italicIndex := strings.Index(subString, ITALIC_INDICATOR)
	boldIndex := strings.Index(subString, BOLD_INDICATOR)

	// Found at least one indicator
	// When returning the index, always add the startIndex to return the absolute position
	if italicIndex != -1 || boldIndex != -1 {
		// Only italic found
		if italicIndex != -1 && boldIndex == -1 {
			return italicIndex + startIndex, ITALIC
		}

		// Only bold found
		if italicIndex == -1 && boldIndex != -1 {
			return boldIndex + startIndex, BOLD
		}

		// Both found, return smaller index
		if italicIndex < boldIndex {
			return italicIndex + startIndex, ITALIC
		}
		return boldIndex + startIndex, BOLD
	}

	// None found
	return -1, PLAIN
}

func addTextSegment(textSegments []*TextSegment, text string, format Format) []*TextSegment {
	// Ignore empty texts
	if len(text) == 0 {
		return textSegments
	}
	// Check if the last segment already has the same format. In that case only extend the text
	if len(textSegments) > 0 && textSegments[len(textSegments)-1].Format == format {
		textSegments[len(textSegments)-1].Text += text
	} else {
		textSegments = append(textSegments, NewTextSegment(text, format))
	}
	return textSegments
}

func updateFormat(oldFormat, newFormat Format) Format {
	if oldFormat == newFormat {
		// segment ends
		return PLAIN
	}

	if oldFormat == PLAIN {
		return newFormat
	}

	if oldFormat == BOLD_AND_ITALIC {
		if newFormat == BOLD {
			return ITALIC
		}
		if newFormat == ITALIC {
			return BOLD
		}
	}

	if oldFormat == BOLD && newFormat == ITALIC {
		return BOLD_AND_ITALIC
	}
	if oldFormat == ITALIC && newFormat == BOLD {
		return BOLD_AND_ITALIC
	}

	// Will never be called, situations above cover everything
	return PLAIN
}

func containsFormatIndicator(text string, format Format) bool {
	switch format {
	case BOLD:
		return strings.Contains(text, BOLD_INDICATOR)
	case ITALIC:
		return strings.Contains(text, ITALIC_INDICATOR)
	}
	// Should never reach this
	return false
}
