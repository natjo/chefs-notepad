package recipeManager

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGetTitleImagePath(t *testing.T) {
	// Given
	mgr := NewPathManager("./../dev/test-data", "/tmp/chefs-notepad/cache")

	// When
	path := mgr.GetTitleImagePath("Category_1", "Tasty Recipe")

	// Then
	assert.Equal(t, "../dev/test-data/Category_1/Tasty_Recipe/title_image.jpg", path)
}

func TestGetSmallTitleImagePathJpg(t *testing.T) {
	// Given
	mgr := NewPathManager("./../dev/test-data", "/tmp/chefs-notepad/cache")

	// When
	path := mgr.GetSmallTitleImagePath("Category_1", "Tasty Recipe")

	// Then
	assert.Equal(t, "/tmp/chefs-notepad/cache/Category_1/Tasty_Recipe/title_image_small.jpg", path)
}

func TestGetSmallTitleImageURLPath(t *testing.T) {
	// Given
	mgr := NewPathManager("./../dev/test-data", "/tmp/chefs-notepad/cache")

	// When
	url := mgr.GetSmallTitleImageURLPath("Category_1", "Tasty Recipe")

	// Then
	assert.Equal(t, "/Category_1/Tasty_Recipe/title_image_small.jpg", url)
}

func TestGetTitleImageCreationDate(t *testing.T) {
	// Given
	mgr := NewPathManager("./../dev/test-data", "/tmp/chefs-notepad/cache")

	// When
	creationDate := mgr.GetTitleImageCreationDate("Category_1", "Tasty Recipe")

	// Then
	assert.Less(t, int64(1703152893), creationDate)
}
