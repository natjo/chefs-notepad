package recipeManager

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestRenderRecipe(t *testing.T) {
	// Given
	file, _ := os.Open("./../dev/test-data/Category_1/Tasty_Recipe/recipe.md")
	defer file.Close()

	// When
	renderedRecipe, infoTags := renderRecipe(file)

	// Then
	// I split the expected text into multiple parts, as the ordering of the popovers doesn't matter, as long as they exist
	expectedRenderedRecipe := `<h1>Ingredients</h1>
<h2>For the dough</h2>
<p><em>(some comment here)</em></p>
<ul>
  <li>250g <b>butter</b></li>
  <li>230g sugär</li>
  <li>4 eggs</li>
</ul>
<h2>For the topping</h2>
<ul>
  <li>3 eggs</li>
  <li>170g sugar</li>
</ul>
<hr>
<h1>Steps</h1>
<p>Lorem <em>ipsum dolor sit</em> amet and the <button value="id-sugär" class="popover-indicator">sugär</button>, consectetur_adipiscing elit, sed do eiusmod tempor <b>incididunt ut</b> labore*et dolore magna aliqua.</p>
<h2>Dough</h2>
<p>Lorem ipsum dolor sit amet, consectetur <button value="id-eggs" class="popover-indicator">eggs</button> adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
<p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
<p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
<p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
<h2>Topping</h2>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.  Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
<hr>
<p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
`
	expectedPopover0 := `<div popover id="id-sugär" class="popover-display">230g</div>`
	expectedPopover1 := `<div popover id="id-eggs" class="popover-display">2</div>`
	expectedPopover2 := `<div popover id="id-sugar" class="popover-display">170g</div>`
	expectedInfoTags := map[string]string{"key0": "val0", "key1": "val1", "a": "b"}

	assert.Contains(t, renderedRecipe, expectedRenderedRecipe)
	assert.Contains(t, renderedRecipe, expectedPopover0)
	assert.Contains(t, renderedRecipe, expectedPopover1)
	assert.Contains(t, renderedRecipe, expectedPopover2)
	assert.Equal(t, expectedInfoTags, infoTags)

}

func TestGetInfoTag(t *testing.T) {
	// Given
	line := "[key: value]"

	// When
	key, value, err := getInfoTag(line)

	// Then
	assert.Nil(t, err)
	assert.Equal(t, "key", key)
	assert.Equal(t, "value", value)
}

func TestGetInfoTagNoWhitespace(t *testing.T) {
	// Given
	line := "[key:value]"

	// When
	key, value, err := getInfoTag(line)

	// Then
	assert.Nil(t, err)
	assert.Equal(t, "key", key)
	assert.Equal(t, "value", value)
}

func TestGetInfoTagExcessiveWhitespace(t *testing.T) {
	// Given
	line := "[ key  :value      ]"

	// When
	key, value, err := getInfoTag(line)

	// Then
	assert.Nil(t, err)
	assert.Equal(t, "key", key)
	assert.Equal(t, "value", value)
}

func TestGetInfoTagMultipleColon(t *testing.T) {
	// Given
	line := "[key: value:value]"

	// When
	key, value, err := getInfoTag(line)

	// Then
	assert.Nil(t, err)
	assert.Equal(t, "key", key)
	assert.Equal(t, "value:value", value)
}

func templateTestGetQuantity(t *testing.T, input, expQuantity, expIngredient string, expOk bool) {
	// Given
	// When
	quantity, ingredient, ok := getQuantity(input)

	// Then
	assert.Equal(t, expQuantity, quantity)
	assert.Equal(t, expIngredient, ingredient)
	assert.Equal(t, expOk, ok)

}

func TestGetQuantity(t *testing.T) {
	templateTestGetQuantity(t, " 250g sugar", "250g", "sugar", true)
}

func TestGetQuantity3words(t *testing.T) {
	templateTestGetQuantity(t, " 2 packages butter", "2 packages", "butter", true)
}

func TestGetQuantityIgnoreAfterComma(t *testing.T) {
	templateTestGetQuantity(t, " 120 grams of butter, roomtemperature", "120 grams of", "butter", true)
}

func TestGetQuantityKeepDot(t *testing.T) {
	templateTestGetQuantity(t, "1 pkg. butter", "1 pkg.", "butter", true)
}

func TestGetQuantityParanthesis(t *testing.T) {
	templateTestGetQuantity(t, "1l milk (full fat)", "1l", "milk", true)
}

func TestGetQuantitySingleWord(t *testing.T) {
	templateTestGetQuantity(t, "salt", "", "", false)
}

func TestGetQuantityTwoWords(t *testing.T) {
	templateTestGetQuantity(t, "151g brown sugar ((1 pound:brown sugar))", "1 pound", "brown sugar", true)
}

func TestGetQuantityOverwrite(t *testing.T) {
	templateTestGetQuantity(t, "151g brown sugar ((150g:sugar))", "150g", "sugar", true)
}

func TestGetQuantityOverwriteOnlyIngredient(t *testing.T) {
	templateTestGetQuantity(t, "150g sugar ((:butter))", "150g", "butter", true)
}

func TestGetQuantityOverwriteOnlyQuantity(t *testing.T) {
	templateTestGetQuantity(t, "250g melted butter ((150g:))", "150g", "butter", true)
}

func TestGetQuantityOverwriteBadFormat(t *testing.T) {
	templateTestGetQuantity(t, "151g brown sugar ((150:g:sugar))", "", "", false)
}

func TestGetQuantityOverwriteBadFormatLeadingColon(t *testing.T) {
	templateTestGetQuantity(t, "151g brown sugar ((:g:sugar))", "", "", false)
}
