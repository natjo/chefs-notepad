package recipeManager

import (
	"fmt"
	"image"
	"log/slog"
	"mime/multipart"
	"os"
	"path/filepath"
	"sort"
	"strings"
)

// Manages all information regarding recipes.
// - Stores new ones
// - Returns information about existing ones
// - Changes recipes
type RecipeManager struct {
	rootPath           string
	pathMgr            *PathManager
	imageMgr           *ImageManager
	templateRecipePath string
	templateImagePath  string
}

func NewRecipeManager(rootPath, cachePath string) *RecipeManager {
	return &RecipeManager{
		rootPath:           rootPath,
		pathMgr:            NewPathManager(rootPath, cachePath),
		imageMgr:           newImageManager(rootPath, cachePath),
		templateRecipePath: filepath.Join(rootPath, "_template.md"),
		templateImagePath:  filepath.Join(rootPath, "_template.jpg"),
	}
}

// Returns a list of all recipes
func (mgr *RecipeManager) GetCategories() []string {
	categories, err := mgr.getSubdirectories(mgr.rootPath)
	if err != nil {
		slog.Error(err.Error())
		// If we can't find the root data directory, something is very wrong
		os.Exit(1)
	}
	removeUnderscoresFromNames(categories)
	sort.Strings(categories)
	return categories
}

func (mgr *RecipeManager) GetRecipeNames(category string) ([]string, error) {
	// Make sure we only traverse the filesystem downwards
	if strings.HasPrefix(category, ".") {
		return []string{}, fmt.Errorf("invalid Category  %s", category)
	}
	category = addUnderscoresToName(category)
	recipePath := filepath.Join(mgr.rootPath, category)
	recipes, err := mgr.getSubdirectories(recipePath)
	if err != nil {
		return []string{}, err
	}
	removeUnderscoresFromNames(recipes)
	sort.Strings(recipes)
	return recipes, nil
}

func (mgr *RecipeManager) GetRecipeText(category, recipe string, inMarkdown bool) (string, map[string]string, error) {
	file, err := os.Open(mgr.pathMgr.GetTextStoragePath(category, recipe))
	if err != nil {
		slog.Error(err.Error())
		return "", nil, fmt.Errorf("can not find recipe %s in category %s", recipe, category)
	}
	defer file.Close()
	if inMarkdown {
		return readRecipe(file), nil, nil
	}
	htmlRecipe, infoTags := renderRecipe(file)
	return htmlRecipe, infoTags, nil
}

func (mgr *RecipeManager) SaveRecipeText(category, recipe, recipeText string) {
	err := os.WriteFile(mgr.pathMgr.GetTextStoragePath(category, recipe), []byte(recipeText), 0644)
	if err != nil {
		slog.Error(err.Error())
	}
}

func (mgr *RecipeManager) SaveTitleImage(category, recipe string, newImage multipart.File, header *multipart.FileHeader) {
	mgr.imageMgr.SaveTitleImage(category, recipe, newImage, header)
}

func (mgr *RecipeManager) GetTitleImage(category, recipe string) (image.Image, error) {
	return mgr.imageMgr.GetTitleImage(category, recipe)
}

func (mgr *RecipeManager) GetSmallTitleImage(category, recipe string) (image.Image, error) {
	return mgr.imageMgr.GetSmallTitleImage(category, recipe)
}

// Returns true for success, false if recipe already exists
func (mgr *RecipeManager) CreateNewRecipe(category, recipe string) bool {
	if mgr.ExistsRecipe(category, recipe) {
		return false
	}
	mgr.copyTemplate(category, recipe)
	return true
}

func (mgr *RecipeManager) UpdateRecipeName(originalRecipeName, newRecipeName, category string) bool {
	if originalRecipeName != newRecipeName {
		originalRecipePath := mgr.pathMgr.getAbsoluteRecipeDirectory(category, originalRecipeName)
		newRecipePath := mgr.pathMgr.getAbsoluteRecipeDirectory(category, newRecipeName)
		err := os.Rename(originalRecipePath, newRecipePath)
		if err != nil {
			slog.Error(err.Error())
			return false
		}
		return true
	}
	return false
}

func (mgr *RecipeManager) ExistsRecipe(category, recipe string) bool {
	_, _, err := mgr.GetRecipeText(category, recipe, false)
	return err == nil
}

// Returns true for success, false if recipe already exists
func (mgr *RecipeManager) DeleteRecipe(category, recipe string) bool {
	if !mgr.ExistsRecipe(category, recipe) {
		slog.Info(fmt.Sprintf("attempted to delete not existing recipe %s in category %s", recipe, category))
		return true
	}
	absoluteDirectoryPath := mgr.pathMgr.getAbsoluteRecipeDirectory(category, recipe)
	err := os.RemoveAll(absoluteDirectoryPath)
	if err != nil {
		slog.Error(err.Error())
		return false
	}
	return true
}

func (mgr *RecipeManager) copyTemplate(category, recipe string) {
	// Create directory
	absoluteDirectoryPath := mgr.pathMgr.getAbsoluteRecipeDirectory(category, recipe)
	err := os.Mkdir(absoluteDirectoryPath, os.ModePerm)
	if err != nil {
		slog.Error(err.Error())
	}

	// Copy recipe and image template
	recipePath := mgr.pathMgr.GetTextStoragePath(category, recipe)
	imagePath := filepath.Join(absoluteDirectoryPath, "title_image.jpg")
	copyFile(mgr.templateRecipePath, recipePath)
	copyFile(mgr.templateImagePath, imagePath)
}

// Returns name as used for url of image
func (mgr *RecipeManager) GetTitleImageURLPath(category, recipe string) string {
	id := mgr.getTitleImageID(category, recipe)
	return fmt.Sprintf("%s?%d", mgr.pathMgr.GetTitleImageURLPath(category, recipe), id)
}

// Returns name as used for url of small image
func (mgr *RecipeManager) GetSmallTitleImageURLPath(category, recipe string) string {
	id := mgr.getTitleImageID(category, recipe)
	return fmt.Sprintf("%s?%d", mgr.pathMgr.GetSmallTitleImageURLPath(category, recipe), id)
}

func (mgr *RecipeManager) getTitleImageID(category, recipe string) int {
	creationDate := mgr.pathMgr.GetTitleImageCreationDate(category, recipe)
	// Reduce number so it's easier to handle
	return int(creationDate - 1600000000)
}

func (mgr *RecipeManager) getSubdirectories(path string) ([]string, error) {
	dir, err := os.Open(path)
	if err != nil {
		return []string{}, err
	}
	defer dir.Close()

	subdirectories, err := dir.Readdirnames(0)
	if err != nil {
		return []string{}, err
	}
	subdirectories = removeTemplates(subdirectories)
	return subdirectories, nil
}

func removeUnderscoresFromNames(names []string) {
	for i, name := range names {
		if strings.ContainsAny(name, "_") {
			names[i] = strings.Replace(name, "_", " ", -1)
		}
	}
}

func addUnderscoresToName(name string) string {
	if strings.ContainsAny(name, " ") {
		return strings.Replace(name, " ", "_", -1)
	}
	return name
}

// We store the template files on the top level, next to our categories,
// but we should be able to filter this out
func removeTemplates(s []string) []string {
	for i := 0; i < len(s); i++ {
		if s[i] == "_template.md" {
			s = append(s[:i], s[i+1:]...)
			// In case we removed the very first one, do not reduce the index
			if i > 0 {
				i--
			}
		}
		if s[i] == "_template.jpg" {
			s = append(s[:i], s[i+1:]...)
			i--
		}
	}
	return s
}
