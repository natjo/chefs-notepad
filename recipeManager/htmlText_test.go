package recipeManager

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestDecorateText(t *testing.T) {
	// Given
	ht := NewHtmlText()
	ht.quantityInfo = map[string]string{"egg": "one"}
	text := "This is some very, very well decorated (text). We need one egg, yes!"
	popoverKeyword := fmt.Sprintf(popoverKeywordTemplate, "egg", "egg")
	expText := fmt.Sprintf("This is some very, very well decorated (text). We need one %s, yes!", popoverKeyword)

	// When
	decoratedText := ht.decorateText(text)

	// Then
	assert.Equal(t, expText, decoratedText)
}

func TestDecorateTextStartEnd(t *testing.T) {
	// Given
	ht := NewHtmlText()
	ht.quantityInfo = map[string]string{"is": "idc"}
	text := "Is this and this is"
	popoverKeyword := fmt.Sprintf(popoverKeywordTemplate, "is", "is")
	popoverKeywordCap := fmt.Sprintf(popoverKeywordTemplate, "is", "Is")
	expText := fmt.Sprintf("%s this and this %s", popoverKeywordCap, popoverKeyword)

	// When
	decoratedText := ht.decorateText(text)

	// Then
	assert.Equal(t, expText, decoratedText)
}

func TestDecorateTextNoParts(t *testing.T) {
	// Given
	ht := NewHtmlText()
	ht.quantityInfo = map[string]string{"is": "idc"}
	text := "This is some very, very well decorated (text). We need one egg, yes!"
	popoverKeyword := fmt.Sprintf(popoverKeywordTemplate, "is", "is")
	expText := fmt.Sprintf("This %s some very, very well decorated (text). We need one egg, yes!", popoverKeyword)

	// When
	decoratedText := ht.decorateText(text)

	// Then
	assert.Equal(t, expText, decoratedText)
}

func TestDecorateTextIgnoreCase(t *testing.T) {
	// Given
	ht := NewHtmlText()
	ht.quantityInfo = map[string]string{"is": "idk"}
	text := "This Is some very, very well decorated (text). We need one egg, yes!"
	popoverKeyword := fmt.Sprintf(popoverKeywordTemplate, "is", "Is")
	expText := fmt.Sprintf("This %s some very, very well decorated (text). We need one egg, yes!", popoverKeyword)

	// When
	decoratedText := ht.decorateText(text)

	// Then
	assert.Equal(t, expText, decoratedText)
}

func TestDecorateTextWithWhitespace(t *testing.T) {
	// Given
	ht := NewHtmlText()
	ht.quantityInfo = map[string]string{"brown sugar": "150g"}
	text := "This is some very, very well decorated (text). We need brown sugar, yes!"
	popoverKeyword := fmt.Sprintf(popoverKeywordTemplate, "brown-sugar", "brown sugar")
	expText := fmt.Sprintf("This is some very, very well decorated (text). We need %s, yes!", popoverKeyword)

	// When
	decoratedText := ht.decorateText(text)

	// Then
	assert.Equal(t, expText, decoratedText)
}

func TestDecorateTextWithLabel(t *testing.T) {
	// Given
	ht := NewHtmlText()
	ht.quantityInfo = map[string]string{"water": "2l"}
	text := "This is some very, very (well)(water) decorated (text). We need one egg, yes!"
	popoverKeyword := fmt.Sprintf(popoverKeywordTemplate, "water", "well")
	expText := fmt.Sprintf("This is some very, very %s decorated (text). We need one egg, yes!", popoverKeyword)

	// When
	decoratedText := ht.decorateText(text)

	// Then
	assert.Equal(t, expText, decoratedText)
}

func TestAppendPopoverTexts(t *testing.T) {
	// Given
	ht := NewHtmlText()
	ht.quantityInfo = map[string]string{"egg": "one"}
	expectedPopovers := fmt.Sprintf(popoverTemplate, "egg", "one") + "\n"

	// When
	ht.appendPopoverTexts()

	// Then
	assert.Equal(t, expectedPopovers, ht.finalText)
}
