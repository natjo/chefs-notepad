package recipeManager

import (
	"bytes"
	"errors"
	"fmt"
	"image"
	"image/jpeg"
	"image/png"
	"io"
	"log/slog"
	"math"
	"mime/multipart"
	"os"
	"path/filepath"

	"golang.org/x/image/draw"
)

// Used to manage image interaction
type ImageManager struct {
	pathMgr                *PathManager
	smallImageMaxDimension int
}

func newImageManager(rootPath, cachePath string) *ImageManager {
	return &ImageManager{
		pathMgr:                NewPathManager(rootPath, cachePath),
		smallImageMaxDimension: 640,
	}
}

// Saves the original image and a smaller size of the image in cache
func (mgr *ImageManager) SaveTitleImage(category, recipe string, newImage multipart.File, header *multipart.FileHeader) {
	originalTitleImagePath := mgr.pathMgr.GetTitleImagePath(category, recipe)
	newImageExtension := filepath.Ext(header.Filename)
	newTitleImageName := fmt.Sprintf("title_image%v", newImageExtension)
	newTitleImagePath := filepath.Join(mgr.pathMgr.getAbsoluteRecipeDirectory(category, recipe), newTitleImageName)

	// To have no hassle with the image extensions, we simply delete the original one
	deleteFile(originalTitleImagePath)
	storeFile(newTitleImagePath, newImage)

	// Create a copy of the image in smaller size as cache
	mgr.createSmallTitleImage(category, recipe)
}

func (mgr *ImageManager) GetTitleImage(category, recipe string) (image.Image, error) {
	titleImagePath := mgr.pathMgr.GetTitleImagePath(category, recipe)
	return mgr.getImageFromPath(titleImagePath)
}

func (mgr *ImageManager) GetSmallTitleImage(category, recipe string) (image.Image, error) {
	smallTitleImagePath := mgr.pathMgr.GetSmallTitleImagePath(category, recipe)
	mgr.ensureSmallTitleImageExists(category, recipe)
	return mgr.getImageFromPath(smallTitleImagePath)
}

func (mgr *ImageManager) getImageFromPath(imagePath string) (image.Image, error) {
	f, err := os.Open(imagePath)
	if err != nil {
		slog.Error(err.Error())
		return nil, err
	}
	defer f.Close()
	image, _, err := image.Decode(f)
	if err != nil {
		slog.Error(err.Error())
		return nil, err
	}
	return image, err
}

func (mgr *ImageManager) ensureSmallTitleImageExists(category, recipe string) {
	smallTitleImagePath := mgr.pathMgr.GetSmallTitleImagePath(category, recipe)
	if _, err := os.Stat(smallTitleImagePath); errors.Is(err, os.ErrNotExist) {
		mgr.createSmallTitleImage(category, recipe)
	}
}

func (mgr *ImageManager) createSmallTitleImage(category, recipe string) {
	originalTitleImagePath := mgr.pathMgr.GetTitleImagePath(category, recipe)
	smallTitleImagePath := mgr.pathMgr.GetSmallTitleImagePath(category, recipe)
	err := scaleImage(originalTitleImagePath, smallTitleImagePath, mgr.smallImageMaxDimension)
	if err != nil {
		slog.Error(err.Error())
	}
}

func scaleImage(inputImageFilePath, outputImageFilePath string, maxDimension int) error {
	src, err := getSourceImage(inputImageFilePath)
	if err != nil {
		return err
	}

	outputWidth, outputHeight := calcNewDimensions(src.Bounds().Max.X, src.Bounds().Max.Y, maxDimension)

	// Set the expected size that we want
	dst := image.NewRGBA(image.Rect(0, 0, outputWidth, outputHeight))

	// Resize
	draw.NearestNeighbor.Scale(dst, dst.Rect, src, src.Bounds(), draw.Over, nil)

	err = saveImage(dst, outputImageFilePath)
	return err
}

func getSourceImage(path string) (image.Image, error) {
	input, _ := os.Open(path)
	defer input.Close()

	switch filepath.Ext(path) {
	case ".png":
		return png.Decode(input)
	case ".jpg", ".jpeg":
		flRead, err := io.ReadAll(input)
		if err != nil {
			return nil, err
		}

		src, err := jpeg.Decode(bytes.NewReader(flRead))
		return src, err
	default:
		return nil, fmt.Errorf("could not scale image with format %s", filepath.Ext(path))
	}
}

func calcNewDimensions(xIn, yIn, maxDimension int) (int, int) {
	x := float64(xIn)
	y := float64(yIn)
	scale := float64(maxDimension) / math.Max(x, y)
	return int(x * scale), int(y * scale)
}

func saveImage(image *image.RGBA, path string) error {
	err := os.MkdirAll(filepath.Dir(path), os.ModePerm)
	if err != nil {
		return err
	}
	output, _ := os.Create(path)
	defer output.Close()

	switch filepath.Ext(path) {
	case ".png":
		return png.Encode(output, image)
	case ".jpg", ".jpeg":
		return jpeg.Encode(output, image, &jpeg.Options{Quality: 90})
	default:
		return fmt.Errorf("could not scale image with format %s", filepath.Ext(path))
	}
}
