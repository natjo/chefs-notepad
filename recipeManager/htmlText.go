package recipeManager

import (
	"fmt"
	"regexp"
	"strings"
)

const popoverKeywordTemplate = `<button value="id-%s" class="popover-indicator">%s</button>`
const popoverTemplate = `<div popover id="id-%s" class="popover-display">%s</div>`

// Generates html input from read lines, handles details not obvious from the one-by-one reading of the markdown file,
// such as lists, but also formats text further, such as bold and italic
type HtmlText struct {
	finalText        string
	ongoingListItems []string
	quantityInfo     map[string]string
}

func NewHtmlText() *HtmlText {
	return &HtmlText{finalText: "", ongoingListItems: []string{}, quantityInfo: map[string]string{}}
}

func (ht *HtmlText) GetText() string {
	ht.finishList()
	ht.appendPopoverTexts()
	return ht.finalText
}

func (ht *HtmlText) AppendH1(textSegments []*TextSegment) {
	ht.finishList()
	ht.appendWrapped(textSegments[0].Text, "h1")
}

func (ht *HtmlText) AppendH2(textSegments []*TextSegment) {
	ht.finishList()
	ht.appendWrapped(textSegments[0].Text, "h2")
}

func (ht *HtmlText) AppendH3(textSegments []*TextSegment) {
	ht.finishList()
	ht.appendWrapped(textSegments[0].Text, "h3")
}

func (ht *HtmlText) AppendH4(textSegments []*TextSegment) {
	ht.finishList()
	ht.appendWrapped(textSegments[0].Text, "h4")
}

func (ht *HtmlText) AppendDivider() {
	ht.finishList()
	ht.finalText += "<hr>\n"
}

func (ht *HtmlText) AppendP(textSegments []*TextSegment) {
	ht.finishList()
	text := ht.renderTextFromSegments(textSegments, true)
	ht.appendWrapped(text, "p")
}

func (ht *HtmlText) AppendListItem(textSegments []*TextSegment) {
	text := ht.renderTextFromSegments(textSegments, false)
	ht.ongoingListItems = append(ht.ongoingListItems, text)
}

func (ht *HtmlText) UpdateQuantityInfo(quantity, ingredient string) {
	ht.quantityInfo[ingredient] = quantity
}

func (ht *HtmlText) finishList() {
	if len(ht.ongoingListItems) > 0 {
		ht.finalText += "<ul>\n"
		for _, item := range ht.ongoingListItems {
			ht.appendIndentedWrapped(item, "li", 2)
		}
		ht.finalText += "</ul>\n"
		ht.ongoingListItems = []string{}
	}
}

func (ht *HtmlText) appendPopoverTexts() {
	for item, quantity := range ht.quantityInfo {
		id := strings.Replace(item, " ", "-", -1)
		ht.finalText += fmt.Sprintf(popoverTemplate, id, quantity) + "\n"
	}
}

func (ht *HtmlText) appendIndentedWrapped(text, htmlTag string, indentLevel int) {
	ht.finalText += strings.Repeat(" ", indentLevel)
	ht.appendWrapped(text, htmlTag)
}

func (ht *HtmlText) appendWrapped(text, htmlTag string) {
	ht.finalText += ht.wrapText(text, htmlTag) + "\n"
}

func (ht *HtmlText) renderTextFromSegments(textSegments []*TextSegment, decorate bool) string {
	text := ""
	for _, segment := range textSegments {
		if decorate {
			decoratedText := ht.decorateText(segment.Text)
			text += ht.renderText(decoratedText, segment.Format)
		} else {
			text += ht.renderText(segment.Text, segment.Format)
		}
	}
	return text
}

func (ht *HtmlText) decorateText(text string) string {
	// Iterate over all ingredients that can have popups.
	// Replace them with the proper popover button element.
	text = ht.replacePopoverLabel(text)
	text = ht.replacePopoverDirect(text)

	return text

}

func (ht *HtmlText) replacePopoverDirect(text string) string {
	for ingredient := range ht.quantityInfo {
		regex := regexp.MustCompile("(?i)" + ingredient)
		allIndexes := regex.FindAllStringIndex(text, -1)

		// As we replace the string with a different length string, we have to adjust with an offset
		offset := 0
		for _, indexes := range allIndexes {
			preIndex := indexes[0] - 1 + offset
			postIndex := indexes[1] + offset

			// Check if surrounded by non-letters
			if preIndex < 0 || !isLetter(rune(text[preIndex])) {
				if postIndex >= len(text) || !isLetter(rune(text[postIndex])) {
					// Check it is not starting with id-, as this is reserved for html element ids
					if !ht.startsWithPopoverID(text, preIndex) {
						startIndex := indexes[0] + offset
						endIndex := indexes[1] + offset
						insideText := text[startIndex:endIndex]
						popoverToText := ht.generatePopoverToText(insideText, ingredient)
						text = replaceAtPosition(text, popoverToText, startIndex, endIndex)
						offset += len(popoverToText) - (indexes[1] - indexes[0])
					}
				}
			}
		}
	}
	return text
}

func (ht *HtmlText) replacePopoverLabel(text string) string {
	for ingredient := range ht.quantityInfo {
		regex := regexp.MustCompile(fmt.Sprintf(`\([^\)]+\)\(%s\)`, ingredient))
		allIndexes := regex.FindAllStringIndex(text, -1)

		// As we replace the string with a different length string, we have to adjust with an offset
		offset := 0
		for _, indexes := range allIndexes {
			// The elements look like this (name)(labelName)
			startIndex := indexes[0] + offset
			endIndex := indexes[1] + offset
			insideText := text[startIndex+1 : strings.Index(text[startIndex:], ")")+startIndex]
			popoverToText := ht.generatePopoverToText(insideText, ingredient)
			text = replaceAtPosition(text, popoverToText, startIndex, endIndex)
			offset += len(popoverToText) - (indexes[1] - indexes[0])
		}
	}
	return text
}

// Checks if text contains 'id-', where endIndex is the index in the text where we expect the `-`
func (ht *HtmlText) startsWithPopoverID(text string, endIndex int) bool {
	if endIndex-2 < 0 {
		// Less than 3 characters in front of the actual word
		return false
	}
	return text[endIndex-2:endIndex+1] == "id-"
}

func (ht *HtmlText) generatePopoverToText(text, popoverId string) string {
	id := strings.Replace(popoverId, " ", "-", -1)
	return fmt.Sprintf(popoverKeywordTemplate, id, text)
}

func (ht *HtmlText) renderText(text string, format Format) string {
	switch format {
	case PLAIN:
		return text
	case BOLD:
		return ht.wrapText(text, "b")
	case ITALIC:
		return ht.wrapText(text, "em")
	case BOLD_AND_ITALIC:
		text := ht.wrapText(text, "b")
		return ht.wrapText(text, "em")
	}
	// Switch above handles all cases
	return ""
}

func (ht *HtmlText) wrapText(text, htmlTag string) string {
	return "<" + htmlTag + ">" + text + "</" + htmlTag + ">"
}
